#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

while true; do 

   echo OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:
   
   read A_GUESS

   if [[ $A_GUESS < 1 ]]; then
      echo You must enter a number that’s \>\= 1
      continue

   elif [[ $A_GUESS > $THE_MAX_VALUE ]]; then
      echo You must enter a number that’s \<\= $THE_MAX_VALUE
      continue

   elif [[ $A_GUESS > $THE_NUMBER_IM_THINKING_OF ]]; then
      echo No cat... the number I’m thinking of is smaller than $A_GUESS 
      continue

   elif [[ $A_GUESS < $THE_NUMBER_IM_THINKING_OF ]]; then
      echo No cat... the number I’m thinking of is larger than $A_GUESS
      continue

   elif [[ $A_GUESS == $THE_NUMBER_IM_THINKING_OF ]]; then
      echo You got me.
      break

   fi

done

echo "  /\\ ___ /\\"
echo " (  o   o  )" 
echo "  \\  >#<  /"
echo "  /       \\"  
echo " /         \\       ^"
echo "|           |     //"
echo " \\         /    //"
echo "  ///  ///   --"

