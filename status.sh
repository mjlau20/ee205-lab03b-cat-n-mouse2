#!/bin/bash

echo I am
echo $(whoami)

echo The current working directory is
echo $(pwd)

echo The system I am on is
echo $(hostname)

echo The Linux version is
echo $(uname -r)

echo The Linux distribution is
echo $(cat /etc/redhat-release)

echo The system has been up for
echo $(uptime)

echo The amount of disk space I\'m using in KB is
echo $(du -k ~ | tail -1)

